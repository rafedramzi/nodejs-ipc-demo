const { spawn } = require("node:child_process");

const subprocess = spawn("node", ["child_program.js"], {
  // detached: true,
  stdio: ["inherit", "inherit", "inherit", "ipc"],
});

subprocess.on("message", (m) => {
  console.log("PARENT receive message:", m);
});

subprocess.send({ hello: "world" });
