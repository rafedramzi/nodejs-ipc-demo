setTimeout(() => {
  console.log("done");
}, 30000);

process.on("message", (m) => {
  console.log("CHILD got message:", m);
});

process.send({ ping: "pong" });
